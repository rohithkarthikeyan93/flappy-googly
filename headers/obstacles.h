#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>


class obstacles{

public:
	obstacles(sf::Vector2f size) {
		hurdle.setSize(size);
		hurdle.setFillColor(sf::Color::Yellow);
			}

	void drawTo(sf::RenderWindow &window) {
		window.draw(hurdle);
	}

	void setSize(sf::Vector2f size)
	{
		hurdle.setSize(size);
	}

	void setPos(sf::Vector2f newPos) {
		hurdle.setPosition(newPos);
	}

	sf::FloatRect getGlobalBounds() {
		return hurdle.getGlobalBounds();
	}


private:
	sf::RectangleShape hurdle;
	};
